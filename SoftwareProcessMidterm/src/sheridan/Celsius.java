/*
 * Author Trevor D'Souza
 * February 24 2021
 */
package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int argument) {
		return (int) Math.round(( 5 *(argument - 32.0)) / 9.0);
	}
}
