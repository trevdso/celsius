/*
 * Author Trevor D'Souza
 * February 24 2021
 */
package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void fromFahrenheitRegular() {
		int result = Celsius.fromFahrenheit(32);
		assertEquals(0 , result);
	}
	
	@Test
	public void fromFahrenheitException() {
		int result = Celsius.fromFahrenheit(-94);
		assertEquals(-70 , result);
	}
	
	@Test
	public void fromFahrenheitBoundaryIn() {
		int result = Celsius.fromFahrenheit(40);
		assertEquals(4 , result);
	}
	
	@Test
	public void fromFahrenheitBoundaryOut() {
		int result = Celsius.fromFahrenheit(57);
		assertEquals(14 , result);
	}

}
